/*----- PROTECTED REGION ID(Motor.cpp) ENABLED START -----*/
//=============================================================================
//
// file :        Motor.cpp
//
// description : C++ source for the Motor class and its commands.
//               The class is derived from Device. It represents the
//               CORBA servant object which will be accessed from the
//               network. All commands which can be executed on the
//               Motor are implemented in this file.
//
// project :     
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
// Copyright (C): 2018
//                European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                France
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#include <Motor.h>
#include <MotorClass.h>

/*----- PROTECTED REGION END -----*/	//	Motor.cpp

/**
 *  Motor class description:
 *    
 */

//================================================================
//  The following table gives the correspondence
//  between command and method names.
//
//  Command name  |  Method name
//================================================================
//  State         |  Inherited (no method)
//  Status        |  Inherited (no method)
//================================================================

//================================================================
//  Attributes managed is:
//================================================================
//================================================================

namespace Motor_ns
{
/*----- PROTECTED REGION ID(Motor::namespace_starting) ENABLED START -----*/

//	static initializations

/*----- PROTECTED REGION END -----*/	//	Motor::namespace_starting

//--------------------------------------------------------
/**
 *	Method      : Motor::Motor()
 *	Description : Constructors for a Tango device
 *                implementing the classMotor
 */
//--------------------------------------------------------
Motor::Motor(Tango::DeviceClass *cl, string &s)
 : TANGO_BASE_CLASS(cl, s.c_str())
{
	/*----- PROTECTED REGION ID(Motor::constructor_1) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	Motor::constructor_1
}
//--------------------------------------------------------
Motor::Motor(Tango::DeviceClass *cl, const char *s)
 : TANGO_BASE_CLASS(cl, s)
{
	/*----- PROTECTED REGION ID(Motor::constructor_2) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	Motor::constructor_2
}
//--------------------------------------------------------
Motor::Motor(Tango::DeviceClass *cl, const char *s, const char *d)
 : TANGO_BASE_CLASS(cl, s, d)
{
	/*----- PROTECTED REGION ID(Motor::constructor_3) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	Motor::constructor_3
}

//--------------------------------------------------------
/**
 *	Method      : Motor::delete_device()
 *	Description : will be called at device destruction or at init command
 */
//--------------------------------------------------------
void Motor::delete_device()
{
	DEBUG_STREAM << "Motor::delete_device() " << device_name << endl;
	/*----- PROTECTED REGION ID(Motor::delete_device) ENABLED START -----*/
	
	//	Delete device allocated objects
	
	/*----- PROTECTED REGION END -----*/	//	Motor::delete_device
}

//--------------------------------------------------------
/**
 *	Method      : Motor::init_device()
 *	Description : will be called at device initialization.
 */
//--------------------------------------------------------
void Motor::init_device()
{
	DEBUG_STREAM << "Motor::init_device() create device " << device_name << endl;
	/*----- PROTECTED REGION ID(Motor::init_device_before) ENABLED START -----*/
	
	//	Initialization before get_device_property() call
	
	/*----- PROTECTED REGION END -----*/	//	Motor::init_device_before
	
	//	No device property to be read from database
	
	/*----- PROTECTED REGION ID(Motor::init_device) ENABLED START -----*/
	
	//	Initialize device
	
	/*----- PROTECTED REGION END -----*/	//	Motor::init_device
}


//--------------------------------------------------------
/**
 *	Method      : Motor::always_executed_hook()
 *	Description : method always executed before any command is executed
 */
//--------------------------------------------------------
void Motor::always_executed_hook()
{
	DEBUG_STREAM << "Motor::always_executed_hook()  " << device_name << endl;
	/*----- PROTECTED REGION ID(Motor::always_executed_hook) ENABLED START -----*/
	
	//	code always executed before all requests
	
	/*----- PROTECTED REGION END -----*/	//	Motor::always_executed_hook
}

//--------------------------------------------------------
/**
 *	Method      : Motor::read_attr_hardware()
 *	Description : Hardware acquisition for attributes
 */
//--------------------------------------------------------
void Motor::read_attr_hardware(TANGO_UNUSED(vector<long> &attr_list))
{
	DEBUG_STREAM << "Motor::read_attr_hardware(vector<long> &attr_list) entering... " << endl;
	/*----- PROTECTED REGION ID(Motor::read_attr_hardware) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	Motor::read_attr_hardware
}


//--------------------------------------------------------
/**
 *	Method      : Motor::add_dynamic_attributes()
 *	Description : Create the dynamic attributes if any
 *                for specified device.
 */
//--------------------------------------------------------
void Motor::add_dynamic_attributes()
{
	/*----- PROTECTED REGION ID(Motor::add_dynamic_attributes) ENABLED START -----*/
	
	//	Add your own code to create and add dynamic attributes if any
	
	/*----- PROTECTED REGION END -----*/	//	Motor::add_dynamic_attributes
}

//--------------------------------------------------------
/**
 *	Method      : Motor::add_dynamic_commands()
 *	Description : Create the dynamic commands if any
 *                for specified device.
 */
//--------------------------------------------------------
void Motor::add_dynamic_commands()
{
	/*----- PROTECTED REGION ID(Motor::add_dynamic_commands) ENABLED START -----*/
	
	//	Add your own code to create and add dynamic commands if any
	
	/*----- PROTECTED REGION END -----*/	//	Motor::add_dynamic_commands
}

/*----- PROTECTED REGION ID(Motor::namespace_ending) ENABLED START -----*/

//	Additional Methods

/*----- PROTECTED REGION END -----*/	//	Motor::namespace_ending
} //	namespace
