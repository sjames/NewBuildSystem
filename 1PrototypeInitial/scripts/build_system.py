#!/usr/bin/env python

from __future__ import print_function

import sys
import argparse
import os
import shutil
import distutils.dir_util
import cogapp
from xml.etree import ElementTree as ET

verbose = False
recursive = False


def find_all_xmi_files(file_path, xmi_files):

    if os.path.isdir(file_path):
        abs_path = os.path.abspath(file_path)

        if verbose:
            print("Searching for xmi at location:", abs_path)

        if recursive:

            # Walk all the sub directory files and search for xmi files
            for root, dirs, files in os.walk(abs_path):
                for file in files:
                    if file.endswith(".xmi") is True:
                        print("Found " + root + "/" + file)
                        xmi_files[file] = root

        else:

            # Walk the current directory only
            for file in os.listdir(file_path):
                if file.endswith(".xmi") is True:
                    print("Found" + abs_path + "/" + file)
                    xmi_files[file] = abs_path


def run_command(args):
    global verbose
    verbose = args.verbose

    global recursive
    recursive = args.recursive

    # Search for all the xmi files and their location
    xmi_files = {}
    find_all_xmi_files(args.directory, xmi_files)

    cog = cogapp.Cog()

    # Now iterate over the files and contruct a build system for each
    for file, location in xmi_files.items():

        if verbose:
            print("Processing file: " + location + "/" + file)

        # We must open the xmi file to deduce its type, since we use different
        # templates for multi class / device class
        root = ET.parse(location + "/" + file).getroot()
        multi_classes_elements = root.findall("multiClasses")

        cog.options.bReplace = True
        cog.options.bDeleteCode = False
        cog.options.defines["XMI_FILE"] = location + "/" + file

        # Do things a little different for device class / multi class
        if len(multi_classes_elements) > 0:

            if verbose:
                print("Got a multi class xmi file: " + file)

            # Remove the files if requested
            if args.replace_cmake:
                if os.path.exists(location + "/CMakeLists.txt"):
                    os.remove(location + "/CMakeLists.txt")

            if args.replace_factory:
                if os.path.exists(location + "/MultiClassesFactory.cpp"):
                    os.remove(location + "/MultiClassesFactory.cpp")

            # Replace missing files
            if os.path.exists(location + "/CMakeLists.txt") is False:
                if verbose:
                    print("Missing file: " + location + "/CMakeLists.txt, creating...")

                shutil.copyfile(os.environ["BUILD_SYSTEM_ROOT"] + "/multi_class_server/CMakeLists.txt", location + "/CMakeLists.txt")

            if os.path.exists(location + "/MultiClassesFactory.cpp") is False:
                if verbose:
                    print("Missing file: " + location + "/MultiClassesFactory.cpp, creating...")

                shutil.copyfile(os.environ["BUILD_SYSTEM_ROOT"] + "/multi_class_server/MultiClassesFactory.cpp", location + "/MultiClassesFactory.cpp")

            # Run cog
            if verbose:
                print("Running cog on multi class...")

            cog.processOneFile(location + "/CMakeLists.txt")
            cog.processOneFile(location + "/MultiClassesFactory.cpp")

        else:

            if verbose:
                print("Got a device class xmi file: " + file)

            # Remove the files if requested
            if args.replace_cmake:
                if os.path.exists(location + "/CMakeLists.txt"):
                    os.remove(location + "/CMakeLists.txt")

            if args.replace_factory:
                if os.path.exists(location + "/ClassFactory.cpp"):
                    os.remove(location + "/ClassFactory.cpp")

            # Replace missing files
            if os.path.exists(location + "/CMakeLists.txt") is False:
                if verbose:
                    print("Missing file: " + location + "/CMakeLists.txt, creating...")

                shutil.copyfile(os.environ["BUILD_SYSTEM_ROOT"] + "/device_server/CMakeLists.txt", location + "/CMakeLists.txt")

            if os.path.exists(location + "/ClassFactory.cpp") is False:
                if verbose:
                    print("Missing file: " + location + "/ClassFactory.cpp, creating...")

                shutil.copyfile(os.environ["BUILD_SYSTEM_ROOT"] + "/device_server/ClassFactory.cpp", location + "/ClassFactory.cpp")

            # Run cog
            if verbose:
                print("Running cog on device class...")

            cog.processOneFile(location + "/CMakeLists.txt")
            cog.processOneFile(location + "/ClassFactory.cpp")

        # Now copy in the cmake file
        distutils.dir_util.copy_tree(os.environ["BUILD_SYSTEM_ROOT"] + "/cmake", location + "/cmake")


def main():
    """ Main function to handle entry and script arguments

    Returns:
        bool -- False if the script hit an error, True otherwise.
    """

    parser = argparse.ArgumentParser(description="Build system generator", epilog="Construct a device server build system from xmi files")

    parser.add_argument("-v", "--verbose", action="store_true", help="verbose output")
    parser.add_argument("-r", "--recursive", action="store_true", default=False, help="Search for and update all device class projects in subdirectories")
    parser.add_argument("--replace-factory", action="store_true", default=False, help="Remove existing factory cpp file it exists")
    parser.add_argument("--replace-cmake", action="store_true", default=False, help="Remove existing cmake file it exists")
    parser.add_argument("directory", metavar="DIRECTORY", help="Where to search for an xmi file and create/update a device class build systems")
    args = parser.parse_args()

    parser.set_defaults(func=run_command)
    args = parser.parse_args()

    if not "BUILD_SYSTEM_ROOT" in os.environ:
        print("Set the environment variable BUILD_SYSTEM_ROOT to point to the root of the template build system files")
        return False

    return args.func(args)


if __name__ == "__main__":
    if main() is False:
        sys.stdout.write("Command failed\n")
