set(CMAKE_DISABLE_IN_SOURCE_BUILD ON)
set(CMAKE_DISABLE_SOURCE_CHANGES  ON)

if ( ${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR} )
    message( FATAL_ERROR "In-source builds not allowed. Please make a new directory (called a build directory) and run CMake from there." )
endif()

cmake_minimum_required (VERSION 3.0.2)
set(CMAKE_SKIP_RPATH true)
set(CMAKE_VERBOSE_MAKEFILE OFF)
set(CMAKE_COLOR_MAKEFILE ON)

# Setup Project ---------------------

# Project version
set(VERSION_MAJOR "0")
set(VERSION_MINOR "1")
set(VERSION_PATCH "0")
set(VERSION_METADATA "")

# A multi class device server contains no device class of its own, its a wrapper
# around several device classes

# [[[cog
# import cog, os
# from xml.etree import ElementTree as et
# 
# if os.path.isfile(XMI_FILE) is False:
#     cog.error("Missing file: %s" % XMI_FILE)
#
# device_name = et.parse(XMI_FILE).getroot().find("multiClasses").attrib["name"]
# cog.outl("set(DEVICE_SERVER_NAME %s)" % device_name)
# ]]]
# [[[end]]]

# Open the project
project(${DEVICE_SERVER_NAME})

# Add Dependencies  ---------------------

# Before we call add_subdirectory, we redirect shared library output to
# our build directory, so that the resulting binary can run against shared
# objects without the need to manually copy them
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR})

# [[[cog
# import cog, os
# from xml.etree import ElementTree as et
#
# if os.path.isfile(XMI_FILE) is False:
#     cog.error("Missing file: %s" % XMI_FILE)
#
# device_classes = et.parse(XMI_FILE).getroot().findall("multiClasses/classes")
# multi_classes = et.parse(XMI_FILE).getroot().findall("multiClasses/multiclass")
#
# classes = [ ("Lib" + dc.attrib["classname"] + "Static", os.path.relpath(dc.attrib["sourcePath"])) for dc in device_classes ]
# classes = classes + [ ("Lib" + mc.attrib["classname"], os.path.relpath(mc.attrib["sourcePath"])) for mc in multi_classes ]
#
# for _, path in classes:
#     cog.outl("add_subdirectory(%s EXCLUDE_FROM_ALL)" % path)
# 
# cog.outl("set(LIBRARY_COMPONENTS %s)" % " ".join(name for name,_ in classes)) 
# ]]]
# [[[end]]]

# Define device class targets ---------------------

set(MULTI_CLASS Lib${DEVICE_SERVER_NAME})

add_library(${MULTI_CLASS} INTERFACE)
target_link_libraries(${MULTI_CLASS} INTERFACE ${LIBRARY_COMPONENTS})
message(STATUS "Created interface library ${MULTI_CLASS} from ${LIBRARY_COMPONENTS}")

# Define device server target ---------------------

set(DEVICE_SERVER_SOURCE MultiClassesFactory.cpp main.cpp)

add_executable(${DEVICE_SERVER_NAME} ${DEVICE_SERVER_SOURCE})
target_link_libraries(${DEVICE_SERVER_NAME} PRIVATE ${MULTI_CLASS})

# Add a generated version header to the project
include(cmake/VersionConfig.cmake)

message(STATUS "Configured ${DEVICE_SERVER_NAME} with version ${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}${VERSION_METADATA}")    
