#!/bin/bash

# To get local cog install
export PATH=~/.local/lib/python2.7/site-packages:~/.local/bin:$PATH
export PKG_CONFIG_PATH="/segfs/tango/release/debian9/lib/pkgconfig"

# Found this info on stachexchange: 
# https://stackoverflow.com/questions/59895/getting-the-source-directory-of-a-bash-script-from-within
SCRIPT_PATH=$(dirname "$(realpath -s "$BASH_SOURCE")")
export PROTOTYPE_ROOT=$SCRIPT_PATH
export BUILD_SYSTEM_ROOT=$SCRIPT_PATH/templates

export PATH=$SCRIPT_PATH/scripts:$PATH