/*----- PROTECTED REGION ID(BatteryStateMachine.cpp) ENABLED START -----*/
//=============================================================================
//
// file :        BatteryStateMachine.cpp
//
// description : State machine file for the Battery class
//
// project :     Battery
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
// Copyright (C): 2018
//                European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                France
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================

#include <Battery.h>

/*----- PROTECTED REGION END -----*/	//	Battery::BatteryStateMachine.cpp

//================================================================
//  States  |  Description
//================================================================


namespace Battery_ns
{
//=================================================
//		Attributes Allowed Methods
//=================================================


//=================================================
//		Commands Allowed Methods
//=================================================


/*----- PROTECTED REGION ID(Battery::BatteryStateAllowed.AdditionalMethods) ENABLED START -----*/

//	Additional Methods

/*----- PROTECTED REGION END -----*/	//	Battery::BatteryStateAllowed.AdditionalMethods

}	//	End of namespace
