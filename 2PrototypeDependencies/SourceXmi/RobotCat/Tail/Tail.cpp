/*----- PROTECTED REGION ID(Tail.cpp) ENABLED START -----*/
//=============================================================================
//
// file :        Tail.cpp
//
// description : C++ source for the Tail class and its commands.
//               The class is derived from Device. It represents the
//               CORBA servant object which will be accessed from the
//               network. All commands which can be executed on the
//               Tail are implemented in this file.
//
// project :     Tail
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
// Copyright (C): 2018
//                European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                France
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#include <Tail.h>
#include <TailClass.h>

/*----- PROTECTED REGION END -----*/	//	Tail.cpp

/**
 *  Tail class description:
 *    
 */

//================================================================
//  The following table gives the correspondence
//  between command and method names.
//
//  Command name  |  Method name
//================================================================
//  State         |  Inherited (no method)
//  Status        |  Inherited (no method)
//================================================================

//================================================================
//  Attributes managed is:
//================================================================
//================================================================

namespace Tail_ns
{
/*----- PROTECTED REGION ID(Tail::namespace_starting) ENABLED START -----*/

//	static initializations

/*----- PROTECTED REGION END -----*/	//	Tail::namespace_starting

//--------------------------------------------------------
/**
 *	Method      : Tail::Tail()
 *	Description : Constructors for a Tango device
 *                implementing the classTail
 */
//--------------------------------------------------------
Tail::Tail(Tango::DeviceClass *cl, string &s)
 : TANGO_BASE_CLASS(cl, s.c_str())
{
	/*----- PROTECTED REGION ID(Tail::constructor_1) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	Tail::constructor_1
}
//--------------------------------------------------------
Tail::Tail(Tango::DeviceClass *cl, const char *s)
 : TANGO_BASE_CLASS(cl, s)
{
	/*----- PROTECTED REGION ID(Tail::constructor_2) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	Tail::constructor_2
}
//--------------------------------------------------------
Tail::Tail(Tango::DeviceClass *cl, const char *s, const char *d)
 : TANGO_BASE_CLASS(cl, s, d)
{
	/*----- PROTECTED REGION ID(Tail::constructor_3) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	Tail::constructor_3
}

//--------------------------------------------------------
/**
 *	Method      : Tail::delete_device()
 *	Description : will be called at device destruction or at init command
 */
//--------------------------------------------------------
void Tail::delete_device()
{
	DEBUG_STREAM << "Tail::delete_device() " << device_name << endl;
	/*----- PROTECTED REGION ID(Tail::delete_device) ENABLED START -----*/
	
	//	Delete device allocated objects
	
	/*----- PROTECTED REGION END -----*/	//	Tail::delete_device
}

//--------------------------------------------------------
/**
 *	Method      : Tail::init_device()
 *	Description : will be called at device initialization.
 */
//--------------------------------------------------------
void Tail::init_device()
{
	DEBUG_STREAM << "Tail::init_device() create device " << device_name << endl;
	/*----- PROTECTED REGION ID(Tail::init_device_before) ENABLED START -----*/
	
	//	Initialization before get_device_property() call
	
	/*----- PROTECTED REGION END -----*/	//	Tail::init_device_before
	
	//	No device property to be read from database
	
	/*----- PROTECTED REGION ID(Tail::init_device) ENABLED START -----*/
	
	//	Initialize device
	
	/*----- PROTECTED REGION END -----*/	//	Tail::init_device
}


//--------------------------------------------------------
/**
 *	Method      : Tail::always_executed_hook()
 *	Description : method always executed before any command is executed
 */
//--------------------------------------------------------
void Tail::always_executed_hook()
{
	DEBUG_STREAM << "Tail::always_executed_hook()  " << device_name << endl;
	/*----- PROTECTED REGION ID(Tail::always_executed_hook) ENABLED START -----*/
	
	//	code always executed before all requests
	
	/*----- PROTECTED REGION END -----*/	//	Tail::always_executed_hook
}

//--------------------------------------------------------
/**
 *	Method      : Tail::read_attr_hardware()
 *	Description : Hardware acquisition for attributes
 */
//--------------------------------------------------------
void Tail::read_attr_hardware(TANGO_UNUSED(vector<long> &attr_list))
{
	DEBUG_STREAM << "Tail::read_attr_hardware(vector<long> &attr_list) entering... " << endl;
	/*----- PROTECTED REGION ID(Tail::read_attr_hardware) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	Tail::read_attr_hardware
}


//--------------------------------------------------------
/**
 *	Method      : Tail::add_dynamic_attributes()
 *	Description : Create the dynamic attributes if any
 *                for specified device.
 */
//--------------------------------------------------------
void Tail::add_dynamic_attributes()
{
	/*----- PROTECTED REGION ID(Tail::add_dynamic_attributes) ENABLED START -----*/
	
	//	Add your own code to create and add dynamic attributes if any
	
	/*----- PROTECTED REGION END -----*/	//	Tail::add_dynamic_attributes
}

//--------------------------------------------------------
/**
 *	Method      : Tail::add_dynamic_commands()
 *	Description : Create the dynamic commands if any
 *                for specified device.
 */
//--------------------------------------------------------
void Tail::add_dynamic_commands()
{
	/*----- PROTECTED REGION ID(Tail::add_dynamic_commands) ENABLED START -----*/
	
	//	Add your own code to create and add dynamic commands if any
	
	/*----- PROTECTED REGION END -----*/	//	Tail::add_dynamic_commands
}

/*----- PROTECTED REGION ID(Tail::namespace_ending) ENABLED START -----*/

//	Additional Methods

/*----- PROTECTED REGION END -----*/	//	Tail::namespace_ending
} //	namespace
