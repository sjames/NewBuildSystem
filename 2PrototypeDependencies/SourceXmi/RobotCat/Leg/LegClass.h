/*----- PROTECTED REGION ID(LegClass.h) ENABLED START -----*/
//=============================================================================
//
// file :        LegClass.h
//
// description : Include for the Leg root class.
//               This class is the singleton class for
//                the Leg device class.
//               It contains all properties and methods which the 
//               Leg requires only once e.g. the commands.
//
// project :     Leg
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
// Copyright (C): 2018
//                European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                France
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#ifndef LegClass_H
#define LegClass_H

#include <tango.h>
#include <Leg.h>


/*----- PROTECTED REGION END -----*/	//	LegClass.h


namespace Leg_ns
{
/*----- PROTECTED REGION ID(LegClass::classes for dynamic creation) ENABLED START -----*/


/*----- PROTECTED REGION END -----*/	//	LegClass::classes for dynamic creation

/**
 *	The LegClass singleton definition
 */

#ifdef _TG_WINDOWS_
class __declspec(dllexport)  LegClass : public Tango::DeviceClass
#else
class LegClass : public Tango::DeviceClass
#endif
{
	/*----- PROTECTED REGION ID(LegClass::Additionnal DServer data members) ENABLED START -----*/
	
	
	/*----- PROTECTED REGION END -----*/	//	LegClass::Additionnal DServer data members

	public:
		//	write class properties data members
		Tango::DbData	cl_prop;
		Tango::DbData	cl_def_prop;
		Tango::DbData	dev_def_prop;
	
		//	Method prototypes
		static LegClass *init(const char *);
		static LegClass *instance();
		~LegClass();
		Tango::DbDatum	get_class_property(string &);
		Tango::DbDatum	get_default_device_property(string &);
		Tango::DbDatum	get_default_class_property(string &);
	
	protected:
		LegClass(string &);
		static LegClass *_instance;
		void command_factory();
		void attribute_factory(vector<Tango::Attr *> &);
		void pipe_factory();
		void write_class_property();
		void set_default_property();
		void get_class_property();
		string get_cvstag();
		string get_cvsroot();
	
	private:
		void device_factory(const Tango::DevVarStringArray *);
		void create_static_attribute_list(vector<Tango::Attr *> &);
		void erase_dynamic_attributes(const Tango::DevVarStringArray *,vector<Tango::Attr *> &);
		vector<string>	defaultAttList;
		Tango::Attr *get_attr_object_by_name(vector<Tango::Attr *> &att_list, string attname);
};

}	//	End of namespace

#endif   //	Leg_H
