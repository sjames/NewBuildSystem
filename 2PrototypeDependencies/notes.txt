Completed In the Previous
* Migration to pkgconfig
* Version number demo
* Python build generation scripting (Cog)
* Recursive builds
* All device classes built as a library

Completed In This:
* 

All device servers are made from a binary compile with a static or shared library.

Changes made to xmi files:

* Removed absolute paths
* Added multiclass as a class of multiclass projects (Head/Body as class of RobotCat)

BUILD FLAGS 
* CMAKE_BUILD_TYPE "RELEASE"|"DEBUG"