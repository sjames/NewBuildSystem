set(CMAKE_DISABLE_IN_SOURCE_BUILD ON)
set(CMAKE_DISABLE_SOURCE_CHANGES  ON)

if ( ${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR} )
    message( FATAL_ERROR "In-source builds not allowed. Please make a new directory (called a build directory) and run CMake from there." )
endif()

cmake_minimum_required (VERSION 3.0.2)
set(CMAKE_SKIP_RPATH true)
set(CMAKE_VERBOSE_MAKEFILE OFF)
set(CMAKE_COLOR_MAKEFILE ON)

# Setup Project ---------------------

# Project version
set(VERSION_MAJOR "0")
set(VERSION_MINOR "1")
set(VERSION_PATCH "0")
set(VERSION_METADATA "")

# User defined variables
set(ADDITIONAL_SOURCE )
set(ADDITIONAL_LIBRARY_PATHS )
set(ADDITIONAL_INCLUDE_PATHS )
set(ADDITIONAL_LIBRARIES )
set(ADDITIONAL_BUILD_FLAGS )

# Add paths from the command line
list(APPEND ADDITIONAL_INCLUDE_PATHS ${CMAKE_INCLUDE_PATH})
list(APPEND ADDITIONAL_LIBRARY_PATHS ${CMAKE_LIBRARY_PATH})

# The build is based around a device class, which can be built as a device server
# or as a static library to be linked into a multi class server

# [[[cog
# import cog, os
# from xml.etree import ElementTree as et
# 
# if os.path.isfile(XMI_FILE) is False:
#     cog.error("Missing file: %s" % XMI_FILE)
#
# device_name = et.parse(XMI_FILE).getroot().find("classes").attrib["name"]
# cog.outl("set(DEVICE_CLASS_NAME %s)" % device_name)
# ]]]
# [[[end]]]

# Open the project. For convenience name the project after the class, this should 
# keep this component unique in a large multi class server
project(${DEVICE_CLASS_NAME})

# allow pkg-config to search the CMAKE_PREFIX_PATH 
set(PKG_CONFIG_USE_CMAKE_PREFIX_PATH ON)
list(APPEND CMAKE_PREFIX_PATH "/usr")

# Find Dependencies  ---------------------
include(cmake/FindLibraries.cmake)

# Find any libraries the user requested
if(ADDITIONAL_LIBRARIES)
    find_libraries(LIBRARIES ${ADDITIONAL_LIBRARIES} SEARCH_PATHS ${ADDITIONAL_LIBRARY_PATHS})
    set(ADDITIONAL_FOUND_LIBRARIES ${FOUND_LIBRARIES})
endif(ADDITIONAL_LIBRARIES)

# First find tango if it has not already been found. Returns an interface library
# called TangoInterfaceLibrary
set(CMAKE_MODULE_PATH "${CMAKE_MODULE_PATH};${CMAKE_CURRENT_SOURCE_DIR}/cmake")
find_package(Tango)

# Define device class targets ---------------------

set(DEVICE_CLASS Lib${DEVICE_CLASS_NAME})
set(DEVICE_CLASS_STATIC Lib${DEVICE_CLASS_NAME}Static)
set(DEVICE_CLASS_SHARED Lib${DEVICE_CLASS_NAME}Shared)

# [[[cog
# import cog, os
# from xml.etree import ElementTree as et
#
# if os.path.isfile(XMI_FILE) is False:
#     cog.error("Missing file: %s" % XMI_FILE)
#
# additional_files = et.parse(XMI_FILE).getroot().findall("classes/additionalFiles")
# 
# for file in additional_files:
#     cog.outl("list(APPEND ADDITIONAL_SOURCE %s)" % file.attrib["path"])
# ]]]
# [[[end]]]

set(${DEVICE_CLASS}_SOURCE 
    ${DEVICE_CLASS_NAME}.cpp 
    ${DEVICE_CLASS_NAME}Class.cpp 
    ${DEVICE_CLASS_NAME}StateMachine.cpp
    ${ADDITIONAL_SOURCE})

# Static library version of the device class
add_library(${DEVICE_CLASS_STATIC} STATIC ${${DEVICE_CLASS}_SOURCE})
target_compile_options(${DEVICE_CLASS_STATIC} PUBLIC -std=c++11 -Wall -Wextra)

set_target_properties(${DEVICE_CLASS_STATIC} 
    PROPERTIES 
        OUTPUT_NAME ${DEVICE_CLASS_NAME}
        EXCLUDE_FROM_ALL 1)

target_link_libraries(${DEVICE_CLASS_STATIC} 
    PUBLIC TangoInterfaceLibrary 
    PRIVATE ${ADDITIONAL_FOUND_LIBRARIES})

target_include_directories(${DEVICE_CLASS_STATIC} 
    PUBLIC ${CMAKE_CURRENT_LIST_DIR} 
    PRIVATE ${PROJECT_BINARY_DIR} ${ADDITIONAL_INCLUDE_PATHS})

# Shared library version of the device class
add_library(${DEVICE_CLASS_SHARED} SHARED ${${DEVICE_CLASS}_SOURCE})
target_compile_options(${DEVICE_CLASS_SHARED} PUBLIC -std=c++11 -Wall -Wextra)
set_property(TARGET ${DEVICE_CLASS_SHARED} PROPERTY POSITION_INDEPENDENT_CODE 1)

set_target_properties(${DEVICE_CLASS_SHARED} 
    PROPERTIES 
        OUTPUT_NAME ${DEVICE_CLASS_NAME}
        EXCLUDE_FROM_ALL 1
        VERSION ${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}
        SOVERSION ${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH})

target_link_libraries(${DEVICE_CLASS_SHARED} 
    PUBLIC TangoInterfaceLibrary 
    PRIVATE ${ADDITIONAL_FOUND_LIBRARIES})

target_include_directories(${DEVICE_CLASS_SHARED} 
    PUBLIC ${CMAKE_CURRENT_LIST_DIR} 
    PRIVATE ${PROJECT_BINARY_DIR} ${ADDITIONAL_INCLUDE_PATHS})

# Define device server target ---------------------

set(DEVICE_SERVER_NAME ${DEVICE_CLASS_NAME})
set(DEVICE_SERVER_SOURCE ClassFactory.cpp main.cpp)

add_executable(${DEVICE_SERVER_NAME} ${DEVICE_SERVER_SOURCE})
target_link_libraries(${DEVICE_SERVER_NAME} PRIVATE ${DEVICE_CLASS_SHARED})

target_include_directories(${DEVICE_SERVER_NAME} 
    PRIVATE ${PROJECT_BINARY_DIR} ${ADDITIONAL_INCLUDE_PATHS}) 

# Install ---------------------

# Platform install definitions, this is good form and will get our binaries
# into the correct place per platform
include(GNUInstallDirs)

# DEV
# install(TARGETS shared_library LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
# install(FILES src/LibHdb++Cassandra.h  DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/libhdb++cassandra)

#install(TARGETS ${DEVICE_CLASS_SHARED} LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR} NAMELINK_SKIP)

#install(FILES XMI FILE DESTINATION ${CMAKE_INSTALL_DATADIR}/xmi-library)

# Add a generated version header to the project
include(cmake/VersionConfig.cmake)

message(STATUS "Configured ${DEVICE_CLASS_NAME} with version ${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}${VERSION_METADATA}")    