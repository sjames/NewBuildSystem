# This cmake file configures the version parameters for the project

# A custom target that is always built
add_custom_target(${DEVICE_SERVER_NAME}VersionConfig DEPENDS GetVersionInfo)

# This command will always run, but it will not cause a rebuild unless the git data changes
add_custom_command(OUTPUT GetVersionInfo
    COMMAND 
        ${CMAKE_COMMAND} -DSOURCE_DIR=${CMAKE_CURRENT_SOURCE_DIR} 
            -DVERSION_MAJOR=${VERSION_MAJOR}
            -DVERSION_MINOR=${VERSION_MINOR}
            -DVERSION_PATCH=${VERSION_PATCH}
            -DVERSION_METADATA=${VERSION_METADATA}
            -DPROJECT_ROOT=${CMAKE_CURRENT_SOURCE_DIR}
            -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
            -P ${CMAKE_CURRENT_SOURCE_DIR}/cmake/VersionGenerator.cmake
    COMMENT "Generating VersionConfig.h...")

# Add the target to the device server only
add_dependencies(${DEVICE_SERVER_NAME} ${DEVICE_SERVER_NAME}VersionConfig)
